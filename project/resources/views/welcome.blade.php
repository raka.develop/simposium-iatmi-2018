<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>IATMI 2018</title>
    <meta name="description" content="Free Bootstrap 4 Theme by uicookies.com">
    <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">

    <link href="https://fonts.googleapis.com/css?family=Crimson+Text:400,400i,600|Montserrat:200,300,400" rel="stylesheet">

    <link rel="stylesheet" href="{{url('law/assets/css/bootstrap/bootstrap.css')}}">
    <link rel="stylesheet" href="{{url('law/assets/fonts/ionicons/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{url('law/assets/fonts/law-icons/font/flaticon.css')}}">

    <link rel="stylesheet" href="{{url('law/assets/fonts/fontawesome/css/font-awesome.min.css')}}">


    <link rel="stylesheet" href="{{url('law/assets/css/slick.css')}}">
    <link rel="stylesheet" href="{{url('law/assets/css/slick-theme.css')}}">

    <link rel="stylesheet" href="{{url('law/assets/css/helpers.css')}}">
    <link rel="stylesheet" href="{{url('law/assets/css/style.css')}}">
</head>
<body data-spy="scroll" data-target="#pb-navbar" data-offset="200">

<nav class="navbar navbar-expand-lg navbar-dark pb_navbar pb_scrolled-light" id="pb-navbar">
    <div class="container">
        <a class="navbar-brand" href="index.html">IATMI</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#probootstrap-navbar" aria-controls="probootstrap-navbar" aria-expanded="false" aria-label="Toggle navigation">
            <span><i class="ion-navicon"></i></span>
        </button>
        <div class="collapse navbar-collapse" id="probootstrap-navbar">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a class="nav-link" href="#section-home">Home</a></li>
                <li class="nav-item"><a class="nav-link" href="#section-about">About</a></li>
                <li class="nav-item"><a class="nav-link" href="#section-last-news">Last News</a></li>
                <li class="nav-item"><a class="nav-link" href="#section-gallery">Gallery</a></li>
                <li class="nav-item"><a class="nav-link" href="#section-download-file">Download File</a></li>
                <li class="nav-item"><a class="nav-link" href="#section-testimonials">Testimonials</a></li>
                <li class="nav-item"><a class="nav-link" href="#section-contact">Contact</a></li>
            </ul>
        </div>
    </div>
</nav>
<!-- END nav -->

<section  style="background-image: url({{url('law/assets/images/1900x1200_img_7.jpg')}}); height: 100vh" class="pb_cover_v1 text-left cover-bg-black cover-bg-opacity-4" id="section-home">
    <div class="container">
        <div class="row align-items-center justify-content-end">
            <div class="col-md-6  order-md-1">

                <h2 class="heading mb-3">SIMPOSIUM</h2>
                <div class="sub-heading"><p class="mb-5">IATMI 2018</p>
                    <p><a href="#section-contact" role="button" class="btn smoothscroll pb_outline-light btn-xl pb_font-13 p-4 rounded-0 pb_letter-spacing-2">Register Now</a></p>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- END section -->

<section class="pb_section pb_section_v1" data-section="about" id="section-about">
    <div class="container">
        <div class="row justify-content-md-center text-center mb-5">
            <div class="col-lg-12 pr-md-12 pr-sm-0">
                <h2 class="mt-0 heading-border-top mb-3 font-weight-normal">About IATMI 2018</h2>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nisi mauris, aliquet a massa at, semper ornare urna. In venenatis consectetur ex, at volutpat augue tempor id. Duis sit amet bibendum ex. Sed lacinia, libero non convallis aliquam, felis sem hendrerit libero, vitae mollis risus sem ut lorem. Duis dui dui, tempor sed leo id, imperdiet condimentum odio. Nunc quis aliquet sapien. Suspendisse finibus urna sed leo lacinia faucibus. Vivamus enim justo, ultrices a aliquet eu, eleifend in dui. Fusce imperdiet elit non lectus dignissim, nec maximus nisi iaculis. Curabitur placerat mattis massa, eget aliquet tortor mollis a. Nulla porta molestie ipsum posuere tempus. Ut tempus elit in elementum semper. Vivamus consectetur, dui sed imperdiet bibendum, est justo vulputate lorem, ac vestibulum libero enim ut nibh. Aenean augue est, viverra non tellus eu, pulvinar tempor ex.
                </p>
            </div>
        </div>
    </div>
</section>
<!-- END section -->

<section class="pb_section bg-light">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 pr-md-7 pr-sm-0">
                <h2 class="mt-0 heading-border-top font-weight-normal">Padang at Glance</h2>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nisi mauris, aliquet a massa at, semper ornare urna. In venenatis consectetur ex, at volutpat augue tempor id. Duis sit amet bibendum ex. Sed lacinia, libero non convallis aliquam, felis sem hendrerit libero, vitae mollis risus sem ut lorem. Duis dui dui, tempor sed leo id, imperdiet condimentum odio. Nunc quis aliquet sapien. Suspendisse finibus urna sed leo lacinia faucibus. Vivamus enim justo, ultrices a aliquet eu, eleifend in dui. Fusce imperdiet elit non lectus dignissim, nec maximus nisi iaculis. Curabitur placerat mattis massa, eget aliquet tortor mollis a. Nulla porta molestie ipsum posuere tempus. Ut tempus elit in elementum semper. Vivamus consectetur, dui sed imperdiet bibendum, est justo vulputate lorem, ac vestibulum libero enim ut nibh. Aenean augue est, viverra non tellus eu, pulvinar tempor ex.
                </p>
            </div>
            <div class="col-lg-5 pr-md-5 pr-sm-0">
                <embed style="margin-top: 80px; margin-left: 50px; width: 95%; height: 55%;" src="https://www.youtube.com/embed/Jp3YDE5K4ZU">
            </div>
        </div>
    </div>
</section>
<!-- END section -->

<section class="pb_section pb_section_v1">
    <div class="container">
        <div class="row justify-content-md-center text-center mb-5">
            <div class="col-lg-12 pr-md-12 pr-sm-0">
                <h2 class="mt-0 heading-border-top mb-3 font-weight-normal">Pogramme</h2>
            </div>
        </div>
        <div class="row">
            @for($list = 1; $list <= 3; $list++)
                <div class="col-md">
                    <div class="card pb_media_v2 d-block text-center mb-3 bg-light">
                        <div class="card-body">
                            <h3 class="mt-0 pb_font-20"><a href="">Lorem ipsum</a></h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nisi mauris, aliquet a massa at, semper ornare urna.</p>
                        </div>
                    </div>
                </div>
            @endfor
        </div>
    </div>
</section>
<!-- END section -->

<section class="pb_sm_py_cover text-center cover-bg-black cover-bg-opacity-4" style="background-image: url({{url('law/assets/images/1900x1200_img_3.jpg')}})">
    <div class="container">

        <div class="row align-items-center">
            <div class="col-md-12">
                <h2 class="heading mb-3">What are you waiting for</h2>
                <p class="sub-heading mb-5 pb_color-light-opacity-8">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nisi mauris, aliquet a massa at, semper ornare urna.</p>
                <p><a href="#section-contact" role="button" class="btn smoothscroll pb_outline-light p-3 rounded-0 pb_font-13 pb_letter-spacing-2">Register Now</a></p>
            </div>
        </div>

    </div>
</section>
<!-- END section -->

<section class="pb_section" data-section="last-news" id="section-last-news">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="mt-0 heading-border-top font-weight-normal">Last News</h2>
            </div>
        </div>
        <div class="row">
            @for($list = 1; $list <= 4; $list++)
                <div class="col-md-3 col-sm-6" style="margin-top: 10px">
                    <div class="card bg-light">
                        <img class="card-img-top w-100 mx-auto" src="{{url('law/assets/images/square_img_5.jpg')}}" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title mt-0 mb-2">Lores ipsum</h5>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nisi mauris, aliquet a massa at, semper ornare urna...<a href="">read more</a>
                            </p>
                        </div>
                    </div>
                </div>
            @endfor
        </div>
    </div>
</section>
<!-- END section -->

<section class="pb_section">
    <div class="multiple-items pb_slide_v1">
        @for($list = 1; $list <= 4; $list++)
        <div>
            <a class="link-block">
                <img src="{{url('law/assets/images/1900x1200_img_'.$list.'.jpg')}}" alt="" class="img-fluid">
                <div class="slide-text">
                    <h3 class="text-white">Lorem ipsum</h3>
                </div>
            </a>
        </div>
        @endfor
    </div>

</section>
<!-- END section -->


<section class="pb_section pb_bg-half" data-section="gallery" id="section-gallery">
    <div class="container">
        <div class="row justify-content-md-center text-center mb-5">
            <div class="col-lg-7">
                <h2 class="mt-0 heading-border-top font-weight-normal">Gallery</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nisi mauris, aliquet a massa at, semper ornare urna.</p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="single-item pb_slide_v2">
                    @for($items = 1; $items <= 3; $items++)
                    <div>
                        <div class="d-lg-flex d-md-block slide_content">
                            <div class="pb_content-media" style="background-image: url({{url('law/assets/images/1900x1200_img_4.jpg')}});"></div>
                            <div class="slide_content-text text-center">
                                <h3 class="font-weight-normal mt-0 mb-4">Lorem ipsum</h3>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nisi mauris, aliquet a massa at, semper ornare urna. In venenatis consectetur ex, at volutpat augue tempor id. Duis sit amet bibendum ex. Sed lacinia, libero non convallis aliquam, felis sem hendrerit libero, vitae mollis risus sem ut lorem. Duis dui dui, tempor sed leo id, imperdiet condimentum odio. Nunc quis aliquet sapien. Suspendisse finibus urna sed leo lacinia faucibus. Vivamus enim justo, ultrices a aliquet eu, eleifend in dui. Fusce imperdiet elit non lectus dignissim, nec maximus nisi iaculis. Curabitur placerat mattis massa, eget aliquet tortor mollis a. Nulla porta molestie ipsum posuere tempus. Ut tempus elit in elementum semper. Vivamus consectetur, dui sed imperdiet bibendum, est justo vulputate lorem, ac vestibulum libero enim ut nibh. Aenean augue est, viverra non tellus eu, pulvinar tempor ex.
                                </p>
                            </div>
                        </div>
                    </div>
                    @endfor
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END section -->

<section class="pb_section bg-light bg-image with-overlay" data-section="download-file" id="section-download-file" style="background-image: url({{url('law/assets/images/1900x1200_img_2.jpg')}})">
    <div class="container">
        <div class="row justify-content-md-center text-center mb-5">
            <div class="col-lg-7">
                <h2 class="mt-0 heading-border-top light font-weight-normal text-white">Download file</h2>
                <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nisi mauris, aliquet a massa at, semper ornare urna.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="card text-center pb_card_v1 mb-4">
                    <div class="card-body">
                        <h5 class="card-title mt-0 mb-2"><a href="">download file registration</a></h5>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card text-center pb_card_v1 mb-4">
                    <div class="card-body">
                        <h5 class="card-title mt-0 mb-2"><a href="">download file template abstract</a></h5>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card text-center pb_card_v1 mb-4">
                    <div class="card-body">
                        <h5 class="card-title mt-0 mb-2"><a href="">download file program book</a></h5>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card text-center pb_card_v1 mb-4">
                    <div class="card-body">
                        <h5 class="card-title mt-0 mb-2"><a href="">download file proposal sponsorship</a></h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="pb_section pb_testimonial_v1" data-section="testimonials" id="section-testimonials">
    <div class="container">
        <div class="row justify-content-md-center text-center mb-5">
            <div class="col-lg-7">
                <h2 class="mt-0 heading-border-top font-weight-normal">What they say about IATMI</h2>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-md-10 col-sm-12 mb-5">
                <div class="single-item-no-arrow pb_slide_v1">
                    @for($list = 1; $list <= 5; $list++)
                    <div>
                        <div class="media">
                            <img class="d-flex img-fluid rounded-circle mb-sm-5" src="{{url('law/assets/images/square_img_5.jpg')}}" alt="Generic placeholder image">
                            <div class="media-body pl-md-5 pl-sm-0">
                                <blockquote>
                                    <p>&ldquo;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nisi mauris, aliquet a massa at, semper ornare urna.&rdquo;</p>
                                    <p class="pb_author">
                                        <cite class="text-uppercase">Lorem ipsum</cite>
                                        <small>Alumni IATMI</small>
                                    </p>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                    @endfor
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END section -->

<section class="pb_section bg-light" data-section="contact" id="section-contact">
    <div class="container">

        <div class="row justify-content-md-center text-center mb-5">
            <div class="col-lg-7">
                <h2 class="mt-0 heading-border-top font-weight-normal">Any question</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nisi mauris, aliquet a massa at, semper ornare urna.</p>
            </div>
        </div>


        <div class="row">
            <div class="col-md-8 pr-md-5 pr-sm-0 mb-4">
                <form action="#">
                    <div class="row">
                        <div class="col-md">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control p-3 rounded-0" id="name">
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="text" class="form-control p-3 rounded-0" id="email">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="message">Message</label>
                        <textarea rows="10" class="form-control p-3 rounded-0" id="message"></textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary pb_font-13 pb_letter-spacing-2 p-3 rounded-0">Send Message</button>
                    </div>
                </form>
            </div>
            <div class="col-md-4">
                <ul class="pb_contact_details_v1">
                    <li>
                        <span class="text-uppercase">Email</span>
                        info@iatmi.com
                    </li>
                    <li>
                        <span class="text-uppercase">Telephone</span>
                        (021) 536 69065
                    </li>
                    <li>
                        <span class="text-uppercase">Fax</span>
                        (021) 536 69065
                    </li>
                    <li>
                        <span class="text-uppercase">Address</span>
                        Jl. Anggrek Garuda Raya Blok I No. 3-4 Slipi <br>
                        RT.9/RW.2, Kemanggisan, Palmerah <br>
                        Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11480
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- END section -->

<footer style="padding: 1em 0; background-color: #34495e" class="pb_footer" role="contentinfo">
    <div class="container">
        <div class="row text-center">
            <div class="col">
                <ul class="list-inline">
                    <li class="list-inline-item"><a href="#" class="p-2"><i class="fa fa-facebook"></i></a></li>
                    <li class="list-inline-item"><a href="#" class="p-2"><i class="fa fa-twitter"></i></a></li>
                    <li class="list-inline-item"><a href="#" class="p-2"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col text-center">
                <p class="pb_font-14">&copy; 2018 <a href="">SIMPOSIUM IATMI 2018</a>. All Rights Reserved.</p>
            </div>
        </div>
    </div>
</footer>

<!-- loader -->
<div id="pb_loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#FDA04F"/></svg></div>


<script src="{{url('law/assets/js/jquery.min.js')}}"></script>

<script src="{{url('law/assets/js/popper.min.js')}}"></script>
<script src="{{url('law/assets/js/bootstrap.min.js')}}"></script>
<script src="{{url('law/assets/js/slick.min.js')}}"></script>

<script src="{{url('law/assets/js/jquery.waypoints.min.js')}}"></script>
<script src="{{url('law/assets/js/jquery.easing.1.3.js')}}"></script>

<script src="{{url('law/assets/js/main.js')}}"></script>

</body>
</html>